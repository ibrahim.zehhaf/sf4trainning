Codage
-----------
* Respect des principes [SOLID](https://fr.wikipedia.org/wiki/SOLID_(informatique))
On se concentre sur les principes suivants en particulier 
  * **Responsabilité unique (single responsibility principle)**
  une classe, une fonction ou une méthode doit avoir une et une seule responsabilité
  * **Ouvert/fermé (open/closed principle)**
  une entité applicative (class, fonction, module ...) doit être ouverte à l'extension, mais fermée à la modification
  * **Ségrégation des interfaces (interface segregation principle)**
  préférer plusieurs interfaces spécifiques pour chaque client plutôt qu'une seule interface générale


* Un controller ne doit pas contenir de la logique métier
* On préfère les annotations pour la partie routing, validation et ORM
* Toute exception métier ou technique doit être logguée 

Configuration
-----------

### Paramètres

Les clés des paramètres doivent commencer par le préfix **app.** et les mots séparés par un underscore, exemple :
```
parameters:
    app.admin_mail: exemple@exemple.com
```
### Services
Utiliser au maximum l'autowiring

Traduction
-----------

### Fichiers

- **messages.\<lang>.yml** pour tous les messages.
- **validator.\<lang>.yml** pour les messages de validation.

### Formatage

- Les clés sont séparés par un underscore
- Tabulations de 4 espaces.

### Clés de traduction pour messages.fr.yml

Les clés de traductions doivent être composées de 3 niveaux (`section.type.nom`) :

1. Section: Pour grouper les clés logiquement.
2. Type: titre, action, texte, libellé de formulaire, alerte...
3. Nom: Elle doit donner une idée du message.

#### Sections

Le premier niveau permet de grouper les clés. Une section peut correspondre à une section du site, à une page, 
à un widget, à une partie des pages (header, footer)... On utilise la section **common** pour les clés de traductions 
communes (save, back, delete).

#### Types

- **title**: Les titres qu'ils soient dans la balise `<title>`, dans la page (`<h1>`, `<h2>`, ...) et dans
les menus.
- **action**: Les actions (Éditer, Enregistrer, Aller à l'accueil...).
- **label**: Les labels des formulaires.
- **placeholder**: Les placeholders pour les champs input/textarea.
- **alert**: Pour les messages d'avertissement, d'erreur, de succès ou les confirmations (Êtes-vous sûr ?)
- **text**: Pour le contenu texte.
