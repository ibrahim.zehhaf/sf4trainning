<?php

namespace App\EventListener;


use App\Exception\GatewayExceptionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class GatewayExceptionListener
{
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getException();

        $response = new Response();

        if ($exception instanceof GatewayExceptionInterface) {
            $this->logger->log($exception->getLevel(), $exception->getMessage(), [
                "pays" => $exception->getCountry(),
                "passerelle" => $exception->getGateway(),
                "reference_bien" => $exception->getRealEstateId(),
                "code_erreur" => $exception->getCode(),
                "trace" => $exception->getCode(),
            ]);
            $response->setContent($exception->getMessage());
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $event->setResponse($response);
    }
}