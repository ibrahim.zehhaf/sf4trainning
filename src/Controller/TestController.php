<?php
namespace App\Controller;

use App\Exception\GatewayRedisException;
use App\Form\TestType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Aws\S3\S3Client;

class TestController extends AbstractController
{

    /**
     * @Route("/test-aws", name="home")
     * @param Request $request
     * @return Response
     */
    function testAws(Request $request, S3Client $s3)
    {
        $form = $this->createForm(TestType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $s3->putObject([
                'Bucket' => 'testzibra',
                'Key' => time() . '_' . $form['file']->getData()->getClientOriginalName(),
                'SourceFile' => $form['file']->getData()->getPathname()
            ]);

            return $this->redirectToRoute('home');
        }

        return $this->render('home.html.twig', [
                'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/test-gateway-exception", name="home")
     * @param Request $request
     * @return Response
     */
    function testGatewayException(Request $request)
    {

        //throw new GatewayRedisException("italie", "iad_italia_ads", 23456);

        return $this->render('home.html.twig');
    }
}
