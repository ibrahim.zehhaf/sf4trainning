<?php

    namespace App\Exception;

class GatewayRedisException extends
    \RuntimeException
    implements
    GatewayExceptionInterface
{
    protected $message = "Redis inaccessible";
    protected $code = 12345;
    protected $country;
    protected $gateway;
    protected $realEstateId;
    protected $level = "EMERGENCY";
    protected $kind = "internal";
    protected $trace;

    public function __construct($country, $gateway, $realEstateId = null)
    {
        parent::__construct($this->message, $this->code, null);
        $this->country = $country;
        $this->gateway = $gateway;
        $this->realEstateId = $realEstateId;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return mixed
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * @return string
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * @return mixed
     */
    public function getRealEstateId()
    {
        return $this->realEstateId;
    }

}