.RECIPEPREFIX +=
PATH  := node_modules/.bin:bin/:$(PATH)

install:
    composer install -o -n
    npm install
    #encore production

test:
    bin/phpunit

cc:
    bin/console cache:clear --no-warmup
    bin/console cache:warmup

watch:
    encore dev --watch

