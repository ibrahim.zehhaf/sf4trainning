<?php

namespace App\Tests\Util;

use App\Util\Test;
use PHPUnit\Framework\TestCase;

class TestTest extends TestCase
{

    public function testAdd()
    {
        $this->assertEquals(42,  (new Test())->add(30, 12));
    }

}